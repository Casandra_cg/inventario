<?php


class Producto extends Conexion

{
    public $id;
    public $nombre;
    public $caducidad;
    public $cantidad;
    public $costo;

//funcion que te muestra todos os registros en la bse de datos
    static function mostrar(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT *FROM productos");
        $pre -> execute();
        $res = $pre -> get_result();

        $productos = [];
        while ($producto = $res ->fetch_object(Producto::class)){
            //Devuelve los elementos
            array_push($productos,$producto);
        }
        return $productos;
    }
    //Funcion que te ayuda a saber que productos ya van a caducar
    static function caducidad(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT id FROM productos where caducidad between curdate() and date_add(curdate(), interval 5 day)");
        $pre -> execute();
        $res = $pre -> get_result();// obtien todo los resultador

        $productosCaducos = [];
        while ($productoC = $res ->fetch_object(Producto::class)){
            //Devuelve los elementos
            array_push($productosCaducos,$productoC);
        }
        return $productosCaducos;
    }
//funcion para buscar un producto por medio de id
    static function buscarId($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me -> con, "SELECT *FROM productos WHERE id =?");
        $pre -> bind_param("i", $id);
        $pre -> execute();
        $res = $pre -> get_result();

        return $res -> fetch_object(Producto::class);
    }

//funcion para insertar los productos en la bse de datos
    function insertar(){
        $pre = mysqli_prepare($this->con, "INSERT INTO productos (nombre,caducidad,cantidad,costo) VALUES (?,?,?,?)");
        $pre -> bind_param("ssii", $this -> nombre, $this -> caducidad, $this -> cantidad, $this -> costo);
        $pre -> execute();
        $pre= mysqli_prepare($this->con, "SELECT LAST id");
    }
//funcion para actualizar los productos
    function update(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "UPDATE productos SET  nombre=?, caducidad=?, cantidad=?, costo=? WHERE id=?");
        $pre -> bind_param("ssiii", $this -> nombre, $this -> caducidad, $this -> cantidad, $this -> costo, $this->id);
        $pre ->execute();
        return true;
    }
//funcion para remover un producto
    static function remove ($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "DELETE FROM productos WHERE id=?");
        $pre-> bind_param("i",$id);
        $pre -> execute();
        return true;
    }

}