<?php
include "app/Models/Conexion.php";
include "app/Models/Producto.php";

class ProductoController
{
    public function __construct()
    {

    }
//funcion para requrir la vita y pasa los metodos necesarios para esto
    public function productos()
    {
        $id = $_GET["id"];

        $Productos = Producto::mostrar();

        $Producto = Producto::buscarId($id);
        $productosCaducos=Producto::caducidad();
        require_once "app/Views/productos.php";
    }

//funcion para pasar los datos que se obtien por metodo post para agregar un registro

    public function agregar()
    {
        if (isset($_POST)) {
            $producto = new Producto();

            $producto->nombre = $_POST["nombre"];
            $producto->caducidad = $_POST["caducidad"];
            $producto->cantidad = $_POST ["cantidad"];
            $producto->costo = $_POST["costo"];
            $producto->insertar();
            header('Location: /inventario/Index.php?controller=Producto&action=productos&id=1');
        }
    }

    public function all()
    {
        $id = $_GET["id"];
        var_dump($id);
        $Producto = Producto::buscarId($id);
        require_once "app/Views/actualizar.php";
    }
//funcion para identificar por medio del id que elemento se desea eliminar
    public function eliminar()
    {
        $id = $_POST["id"];
        var_dump($id);
        $Producto = Producto::remove($id);
        header('Location: /inventario/Index.php?controller=producto&action=productos&id=1');
    }
//funcion para psar los nuevos parametros paa editar un producto
    public function editar()
    {
        if (isset ($_POST)) {
            $id = $_POST["id"];
            $producto = Producto::buscarId($id);//manda a llamar una clase statica de Articulo
            $producto->nombre = $_POST["nombre"];
            $producto->caducidad = $_POST["caducidad"];
            $producto->cantidad = $_POST ["cantidad"];
            $producto->costo = $_POST["costo"];
            $producto -> id = $id;
            $producto->update();
            header('Location: /inventario/Index.php?controller=Producto&action=all&id='.$id);

        }
    }

}