<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>
<body >
<div class='jumbotron text-center'>
    <h1 class='text-primary'>Editar Producto</h1>
</div>
<div class="container">
    <form action="Index.php?controller=Producto&action=editar" enctype="multipart/form-data" method="POST" id="registro">
        <input id="id" name="id" type="hidden" value="<?php if (isset($Producto)) echo $Producto->id ?>">
        <div class="form-group">
            <div class="form-row">

                <div class="form-group col-md-4">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value='<?php echo $Producto->nombre ?>'>
                </div>
                <div class="form-group col-md-4">
                    <label for="caducidad">Caducidad:</label>
                    <input type="text" class="form-control" id="caducidad" name="caducidad" value='<?php echo $Producto->caducidad?>'>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="cantidad">Cantidad:</label>
                    <input type="text" class="form-control" id="cantidad" name="cantidad" value='<?php echo $Producto->cantidad ?>'">
                </div>
                <div class="form-group col-md-4">
                    <label for="costo">Costo:</label>
                    <input type="text" class="form-control" id="costo" name="costo" value='<?php echo $Producto->costo?>'>
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-4 offset-md-5">
                    <a href="?controller=Producto&action=productos&id=1" class="btn btn-outline-success">Regresar</a>
                    <button class="btn btn-outline-primary" type="submit">Guardar</button>
                </div>
            </div>
        </div>
    </form>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>
