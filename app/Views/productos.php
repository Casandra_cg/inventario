<!doctype html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Productos</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body >
<header>
    <?php
    require_once ('header.php');
    ?>
</header>
<div class='jumbotron text-center'>
    <h1 class='text-primary'>Registrar</h1>
</div>
<div class="container">
    <form action="/inventario/?controller=producto&action=agregar" enctype="multipart/form-data" method="POST" id="registro">

        <div class="form-row">

            <div class="form-group col-md-4">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto">
            </div>
            <div class="form-group col-md-4">
                <label for="caducidad">Caducidad</label>
                <input type="date" class="form-control" id="caducidad" name="caducidad" placeholder="caducidad">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="cantidad">Cantidad</label>
                <input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="cantidad">
            </div>
            <div class="form-group col-md-4">
                <label for="costo">Costo</label>
                <input type="number" class="form-control" id="costo" name="costo" placeholder="Costo ">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4 offset-md-5">
                <button class="btn btn-outline-success" type="submit">Agregar</button>
            </div>
        </div>
    </form>

    <?php
    if (isset($Productos)) {
        if (!empty($Productos)) {
            echo "<div class='container'>" .
                "<h2>INVENTARIO</h2>" .
                "</div>";
            foreach ($Productos as $pro) {

                echo "<div class='card forma' style='margin: 10px'>".
                    "<div class='card-body'>".
                    "<h6 class='card-title'><a href='#'>Id: </a>".$pro->id.
                    "<h6 class='card-title'><a href='#'>Nombre: </a> ".$pro->nombre."</h6>".
                    "<h6 class='card-title'><a href='#'>Caducidad: </a> ".$pro->caducidad." </h6>".
                    "<h6 class='card-title'><a href='#'>Cantidad: </a>".$pro->cantidad."</h6>".
                    "<h6 class='card-title'><a href='#'>Costo: </a>".$pro->costo."</h6>".
                    "<a href='/inventario/?controller=producto&action=all&id=". $pro->id."' class='btn btn-outline-success'>Editar</a>".
                    "<form action='/inventario/?controller=producto&action=eliminar' class='card-text' enctype='multipart/form-data' method='post' id='formEliminar'>".
                    "<input id='id' name='id' type='hidden' value='{$pro-> id}'><br>".
                    "<button id='btn-eliminar'class='btn btn-outline-danger btnEliminar' type='submit'>Eliminar</button>".
                    "</form>".
                    "</div></div>";

            }

        }
        $contador=count($Productos);
        if($contador<5){
            echo '<script language="javascript">alert("Quedan menos de 5 productos en inventario");</script>';
        }
    }
    if (isset($productosCaducos)) {
    if (!empty($productosCaducos)) {
        foreach ($productosCaducos as $proC) {
            $caducos = "el producto con id=" . $proC->id . "va a caducar";

        }
    }
    }

    ?>

</div>

<script type="text/javascript">
    alert( "<?php     foreach ($productosCaducos as $proC) {
        $caducos= "El producto con id:"." ".$proC->id ." "."va a caducar";
        echo $caducos.'\n';
    }?>" );

</script>
</body>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>